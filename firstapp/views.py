#from django.http import HttpResponse, JsonResponse
#from django.views.decorators.csrf import csrf_exempt
#from rest_framework.parsers import JSONParser
from .models import Post, Category
from .serializers import PostSerializer, CategorySerializer
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.decorators import permission_classes


# Create your views here.

# @csrf_exempt
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def post_list(request):
    # NOTE : JE mae chundi a k mae google te json ,api vgera de format ch output vekh ska jive v chandi a ta mnu formatNone likhna pena
    # NOTE : and nal url.py file ch v chnges kityea ne ona nu v comment kita hoya mae othe v chnge krna
    # NOTE : http://127.0.0.1:8000/posts/all.json OR  http://127.0.0.1:8000/posts/all.api
    # NOTE : uper vale link lga k api te jsoon ch data show hona
    # def post_list(request, format=None):
    if request.method == 'GET':
        snippets = Post.objects.all()
        serializer = PostSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        #data = JSONParser().parse(request)
        #serializer = PostSerializer(data=data)
        serializer = PostSerializer(data=request)

        if serializer.is_valid():
            serializer.save()
            # return Response(serializer.data, status=201)
            # return jsonResponse(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        # return Response(serializer.errors, status=400)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


"""
    Retrieve, update or delete a code snippet.
"""
# NOTE : JO NICHE LINK DITE EDE nal har user da vakh vakh detail show honi
#        mere database ch 2 users ne bes islyi mae id 1,2 2 da hi kih k dita ethe
# http://127.0.0.1:8000/posts/1/
# http://127.0.0.1:8000/posts/2/


# @csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def post_detail(request, pk):
    # def post_detail(request, pk, format=None):
    try:
        post = Post.objects.get(pk=pk)
    except Post.DoesNotExist:
        # return HttpResponse(status=404)
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PostSerializer(post)
        # return JsonResponse(serializer.data)
        return Response(serializer.data)

    elif request.method == 'PUT':   # PUT means edit lyi
        #data = JSONParser().parse(request)
        #serializer = PostSerializer(post, data=data)
        serializer = PostSerializer(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            # return JsonResponse(serializer.data)
            return Response(serializer.data)
        # return JsonResponse(serializer.errors, status=400)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        post.delete()
        # return HttpResponse(status=204)
        return Response(status=status.HTTP_204_NO_CONTENT)

from django.urls import path
from .views import post_list, post_detail
from rest_framework.urlpatterns import format_suffix_patterns
# from firstapp import views ,UPER VALI LINE NU EDA V LIKH SAKDE A ASI ede lyi fr niche eda likhna pena path('', views.post_list, name='list'),


urlpatterns = [
    path('', post_list, name='list'),
    #path('all/', post_list, name='list'),
    path('<int:pk>/', post_detail, name="detail"),  # pk de jga id v likh SAKDE
]
#urlpatterns = format_suffix_patterns(urlpatterns)
